package com.devEducation;

import com.devEducation.model.BankAccount;

public class Main {

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount(54000, 67000);
        System.out.println(bankAccount.getMinBalance());
        System.out.println(bankAccount.getCurrentBalance());
        bankAccount.credit(20000, 120, 35);
        bankAccount.credit(20000, 120, 35);
        bankAccount.credit(20000, 120, 35);
        System.out.println(bankAccount.getSumOfCredits());
        System.out.println(bankAccount.getCurrentBalance());
        System.out.println(bankAccount.checkTheAvailabilityOfCredits());
    }
}
