package com.devEducation.utils;

public class Constants {

    public static final int INITIAL_BALANCE = 50000;
    public static final int MAX_BALANCE = 1000000;
    public static final int MIN_CREDIT_AMOUNT = 500;
    public static final int MAX_CREDIT_AMOUNT = 20000;
    public static final int MIN_DEPOSIT_AMOUNT = 1000;
    public static final int MAX_DEPOSIT_AMOUNT = 20000;
    public static final int MIN_OPERATION_PERIOD = 120;
    public static final int MAX_OPERATION_PERIOD = 360;
    public static final int MIN_DEPOSIT_INTEREST = 6;
    public static final int MAX_DEPOSIT_INTEREST = 12;
    public static final int MIN_CREDIT_INTEREST = 24;
    public static final int MAX_CREDIT_INTEREST = 49;
    public static final int TRANSACTION_COUNT = 10000;
}
