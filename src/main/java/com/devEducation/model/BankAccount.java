package com.devEducation.model;

import com.devEducation.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BankAccount {

    private double minBalance;
    private double currentBalance;
    private double depositAmount;
    private double creditAmount;
    private double sumOfDeposits;
    private double sumOfCredits;
    private int numberOfDeposits = 0;
    private int depositPeriod;
    private int creditPeriod;

    private static final Logger logger = LoggerFactory.getLogger(BankAccount.class);

    public BankAccount(double minBalance, double currentBalance) {
        this.minBalance = minBalance;
        this.currentBalance = currentBalance;
    }

    public boolean checkMinBankBalance() {
        return minBalance >= Constants.INITIAL_BALANCE + 4000
                || minBalance >= Constants.INITIAL_BALANCE + 2 * sumOfDeposits / numberOfDeposits;
    }

    public boolean checkTheAvailabilityOfCredits() {
        return creditAmount < currentBalance - minBalance;
    }

    public boolean checkTheAvailabilityOfDeposit() {
        return depositAmount < Constants.MAX_BALANCE - currentBalance;
    }

    public void deposit(double depositAmount, int depositPeriod, double depositInterest) {
        depositAmount = Constants.MIN_DEPOSIT_AMOUNT +
                (int) (Math.random() * (Constants.MAX_DEPOSIT_AMOUNT + 1));
        depositPeriod = (int) (Constants.MIN_OPERATION_PERIOD +
                (Math.random() * (Constants.MAX_OPERATION_PERIOD + 1)));
        depositInterest = Constants.MIN_DEPOSIT_INTEREST +
                (int) (Math.random() * (Constants.MAX_DEPOSIT_INTEREST + 1));
        sumOfDeposits += depositAmount;
        numberOfDeposits++;
        currentBalance += depositAmount;
    }

    public void credit(double creditAmount, int creditPeriod, double creditInterest) {
        creditAmount = Constants.MIN_CREDIT_AMOUNT +
                (int) (Math.random() * (Constants.MAX_CREDIT_AMOUNT + 1));
        creditPeriod = (int) (Constants.MIN_OPERATION_PERIOD +
                (Math.random() * (Constants.MAX_OPERATION_PERIOD + 1)));
        creditInterest = Constants.MIN_CREDIT_INTEREST +
                (int) (Math.random() * (Constants.MAX_CREDIT_INTEREST + 1));
        sumOfCredits += creditAmount;
        currentBalance -= creditAmount;
    }

    public double getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(double minBalance) {
        this.minBalance = minBalance;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public double getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(double creditAmount) {
        this.creditAmount = creditAmount;
    }

    public double getSumOfDeposits() {
        return sumOfDeposits;
    }

    public void setSumOfDeposits(double sumOfDeposits) {
        this.sumOfDeposits = sumOfDeposits;
    }

    public double getSumOfCredits() {
        return sumOfCredits;
    }

    public void setSumOfCredits(double sumOfCredits) {
        this.sumOfCredits = sumOfCredits;
    }

    public int getNumberOfDeposits() {
        return numberOfDeposits;
    }

    public void setNumberOfDeposits(int numberOfDeposits) {
        this.numberOfDeposits = numberOfDeposits;
    }

    public int getDepositPeriod() {
        return depositPeriod;
    }

    public void setDepositPeriod(int depositPeriod) {
        this.depositPeriod = depositPeriod;
    }

    public int getCreditPeriod() {
        return creditPeriod;
    }

    public void setCreditPeriod(int creditPeriod) {
        this.creditPeriod = creditPeriod;
    }
}
