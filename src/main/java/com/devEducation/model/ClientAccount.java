package com.devEducation.model;

public class ClientAccount {

    private int balance;
    private int accountID;

    public ClientAccount(int accountID, int balance) {
        this.accountID = accountID;
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String clientBalance() {
        return "accountID=" + accountID + " : clientBalance=" + balance;
    }
}
